package ru.ermolaev.tm.model;

import ru.ermolaev.tm.constant.ITerminalConst;
import ru.ermolaev.tm.constant.IArgumentConst;

public enum TerminalCommand implements ITerminalConst {

    HELP(ITerminalConst.HELP, IArgumentConst.HELP, "Display terminal commands."),
    ABOUT(ITerminalConst.ABOUT, IArgumentConst.ABOUT, "Show developer info."),
    VERSION(ITerminalConst.VERSION, IArgumentConst.VERSION, "Show version info."),
    INFO(ITerminalConst.INFO, IArgumentConst.INFO, "Show hardware info."),
    EXIT(ITerminalConst.EXIT, null, "Exit from application."),
    COMMANDS(ITerminalConst.COMMANDS, IArgumentConst.COMMANDS, "Show application's commands."),
    ARGUMENTS(ITerminalConst.ARGUMENTS, IArgumentConst.ARGUMENTS, "Show application's arguments.");

    private String command;

    private String argument;

    private String description;

    TerminalCommand(final String command, final String argument, final  String description) {
        this.command = command;
        this.argument = argument;
        this.description = description;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (command != null && !command.isEmpty()) result.append(command);
        if (argument != null && !argument.isEmpty()) result.append(", ").append(argument);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }

}
