package ru.ermolaev.tm;

import ru.ermolaev.tm.controller.TerminalController;
import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        System.out.println("Welcome to task manager");
        if(TerminalController.parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            TerminalController.parseCommand(command);
        }
    }

}
